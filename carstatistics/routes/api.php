<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/status/{car}', 'DashboardController@registerLog')->name('cars.status');

Route::get('/configs/{car}', 'CarController@carConfigs')->name('cars.configs');

/*Route::post('/rpm/{car}', function (Car $car, Request $request) {
$options = array(
'cluster' => 'eu',
'encrypted' => true,
);

$pusher = new Pusher\Pusher(
'd2ff6b2aa855b812db52',
'4e9580d3c23f78b79496',
'450289',
$options
);

$data['message'] = 'hello world';
$data['car'] = $car;
$pusher->trigger('channel-' . $car->user_id, 'status', $data);

// return ($car);
return ($request);
})->name('teste');
 */

//sudo certbot --authenticator webroot --webroot-path /var/www/html/carstatistics/carstatistics/carstatistics/public --installer apache -d carstatisticsngts.ddns.net -d www.carstatisticsngts.ddns.net
