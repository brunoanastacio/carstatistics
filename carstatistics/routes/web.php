<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/**
 * Authentication
 */
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

/**
 * Home and Dashboard
 */
Route::get('/', 'HomeController@index')->name('home');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth');

/**
 * User profiles
 */
Route::get('/my_profile/', 'UserController@showAuthUserProfile')->name('my_profile')->middleware('auth');
Route::get('/profile/{user}', 'UserController@show')->name('profile')->middleware('role:1');
Route::post('/profile/{user}/update', 'UserController@update')->name('user.update')->middleware('auth');

/**
 * Cars section
 */
Route::get('/cars', 'CarController@index')->name('cars')->middleware('auth');
Route::get('/cars/create', 'CarController@create')->name('cars.create')->middleware('auth');
Route::post('/cars/store', 'CarController@store')->name('cars.store')->middleware('auth');
Route::get('/cars/{car}/delete', 'CarController@delete')->name('cars.delete')->middleware('auth');
Route::post('/cars/update', 'CarController@update')->name('car.update')->middleware('auth');

/**
 * Logs section
 */
Route::get('/logs', 'LogController@index')->name('logs')->middleware('auth');
Route::get('/cars/{car}/logs', 'LogController@showCarLogs')->name('cars.logs')->middleware('auth');

/**
 * TESTS SECTION
 */
