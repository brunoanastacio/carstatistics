<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->char('matriculation', 8)->unique();
            $table->string('brand', 20)->nullable();
            $table->string('model', 20)->nullable();
            $table->integer('year')->nullable();
            $table->integer('month')->nullable();
            $table->integer('speed_threshold')->default(10);
            $table->integer('rpm_threshold')->default(500);
            $table->integer('intake_temp_threshold')->default(5);
            $table->integer('speed_max')->default(180);
            $table->integer('rpm_max')->default(5000);
            $table->integer('intake_temp_max')->default(60);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
