<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        Create user
         */
        /*
        $table->increments('id');
        $table->integer('role')->default(0);
        $table->string('name');
        $table->string('email')->unique();
        $table->string('password');
        $table->rememberToken();
        $table->timestamps();
         */

        DB::table('users')->insert([
            'name' => 'Bruno Anastácio',
            'email' => 'brunoanastacio95@gmail.com',
            'password' => bcrypt('123456789'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $this->call(CarsTableSeeder::class);
        $this->call(LogsTableSeeder::class);
    }
}
