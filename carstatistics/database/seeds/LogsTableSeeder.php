<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        $table->increments('id');
        $table->unsignedInteger('car_id');
        $table->string('description');
        $table->float('value');
        $table->enum('key', ['rpm', 'speed']);
        $table->timestamps();
         */

        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'rotacoes por minuto',
            'value' => 1005,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'velocidade',
            'value' => 10,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'rotacoes por minuto',
            'value' => 1050,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'velocidade',
            'value' => 15,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'rotacoes por minuto',
            'value' => 1500,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'velocidade',
            'value' => 60,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'rotacoes por minuto',
            'value' => 3000,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 1,
            'description' => 'velocidade',
            'value' => 150,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);

        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'rotacoes por minuto',
            'value' => 1005,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'velocidade',
            'value' => 10,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'rotacoes por minuto',
            'value' => 1050,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'velocidade',
            'value' => 15,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'rotacoes por minuto',
            'value' => 1500,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'velocidade',
            'value' => 60,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'rotacoes por minuto',
            'value' => 3000,
            'key' => 'rpm',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        sleep(1);
        DB::table('logs')->insert([
            'car_id' => 2,
            'description' => 'velocidade',
            'value' => 150,
            'key' => 'speed',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
