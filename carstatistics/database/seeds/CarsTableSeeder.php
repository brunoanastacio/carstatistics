<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        $table->increments('id');
        $table->unsignedInteger('user_id');
        $table->char('matriculation', 8)->unique();
        $table->string('brand', 20);
        $table->string('model', 20)->nullable();
        $table->integer('year')->nullable();
        $table->integer('month')->nullable();
        $table->timestamps();
         */

        DB::table('cars')->insert([
            'user_id' => 1,
            'matriculation' => '13-OU-06',
            'brand' => 'Honda',
            'model' => 'Civic',
            'year' => 2014,
            'month' => 12,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('cars')->insert([
            'user_id' => 1,
            'matriculation' => '06-BB-06',
            'brand' => 'Audi',
            'model' => 'A4 Avant',
            'year' => 2005,
            'month' => 12,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
