<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{

    public function index()
    {
        $title = "Logs";
        $logs = Auth::user()->lastLogs(1000);
        $cars = Auth::user()->cars;
        return view('logs.index', compact('title', 'logs', 'cars'));
    }

    public function showCarLogs(Car $car)
    {
        $title = "Logs";
        $logs = Auth::user()->logs()->where('car_id', $car->id)->latest()->get();
        return view('logs.car_logs', compact('title', 'logs', 'car'));
    }

}
