<?php

namespace App\Http\Controllers;

use App\Car;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Pusher\Pusher;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Dashboard";
        return view('users.dashboard', compact('title'));
    }

    public function registerLog(Car $car, Request $request)
    {
        $this->validate($request, [
            'value' => 'required|string',
            'key' => 'required|string',
        ]);

        $value = floatval($request['value']);

        $auxLog['key'] = $request['key'];
        $auxLog['value'] = $value;

        $auxCar['brand'] = $car->brand;
        $auxCar['model'] = $car->model;
        $auxCar['matriculation'] = $car->matriculation;
        $auxCar['user_id'] = $car->user_id;

        $this->sendToPusher($auxLog, $auxCar);

        $description = '';

        if ($request['key'] == 'rpm') {
            $description = 'rotacoes por minuto';
            if ($value > $car->rpm_max) {
                $data = ['title' => 'Notificações e alertas!', 'content' => 'Rotações por minuto elevadas!', 'value' => $car->rpm_max];
                Mail::send('layouts.emails.notification', $data, function ($message) use ($car) {
                    $message->from('ngts.cib@gmail.com', 'CarStatistics');
                    $message->to($car->user->email, $car->user->name)->subject('Notificações e Alertas');
                });
            }
        }

        if ($request['key'] == 'speed') {
            $description = 'velocidade';
            if ($value > $car->speed_max) {
                $data = ['title' => 'Notificações e alertas!', 'content' => 'Velocidade elevada!', 'value' => $car->speed_max];
                Mail::send('layouts.emails.notification', $data, function ($message) use ($car) {
                    $message->from('ngts.cib@gmail.com', 'CarStatistics');
                    $message->to($car->user->email, $car->user->name)->subject('Notificações e Alertas');
                });
            }
        }
        if ($request['key'] == 'intake_temp') {
            $description = 'temperatura do ar';
            if ($value > $car->intake_temp_max) {
                $data = ['title' => 'Notificações e alertas!', 'content' => 'Temperatura do ar de entrada elevada!', 'value' => $car->intake_temp_max];
                Mail::send('layouts.emails.notification', $data, function ($message) use ($car) {
                    $message->from('ngts.cib@gmail.com', 'CarStatistics');
                    $message->to($car->user->email, $car->user->name)->subject('Notificações e Alertas');
                });
            }
        }

        $log = Log::create([
            'car_id' => $car->id,
            'value' => $value,
            'key' => $request->key,
            'description' => $description,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        return $log;
    }

    public function sendToPusher($log, $car)
    {
        $options = array(
            'cluster' => 'eu',
            'encrypted' => true,
        );

        $pusher = new Pusher(
            'd2ff6b2aa855b812db52',
            '4e9580d3c23f78b79496',
            '450289',
            $options
        );

        $data['key'] = $log['key'];
        $data['value'] = $log['value'];
        $data['brand'] = $car['brand'];
        $data['model'] = $car['model'];
        $data['matriculation'] = $car['matriculation'];

        $pusher->trigger('channel-' . $car['user_id'], 'status', $data);
    }

}
