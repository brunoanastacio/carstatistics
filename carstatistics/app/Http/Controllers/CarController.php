<?php

namespace App\Http\Controllers;

use App\Car;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Cars";
        $cars = Auth::user()->cars;
        return view('cars.index', compact('title', 'cars'));
    }

    public function create()
    {
        $title = "Cars";
        return view('cars.create', compact('title'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'matriculation' => 'required|string|max:8|min:8',
            'brand' => 'nullable|string|max:250',
            'model' => 'nullable|string|max:250',
            'year' => 'nullable|numeric',
            'month' => 'nullable|numeric',
        ]);

        Car::create([
            'user_id' => Auth::user()->id,
            'matriculation' => $request->matriculation,
            'brand' => $request->brand,
            'model' => $request->model,
            'year' => $request->year,
            'month' => $request->month,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        return redirect()->route('cars')->with('status', 'Carro registado com sucesso!');
    }

    public function delete(Car $car)
    {
        if ($car->user_id == Auth::user()->id) {
            DB::table('logs')->where('car_id', $car->id)->delete();
            $car->delete();
            return redirect()->route('cars')->with('status', 'Carro apagado com sucesso!');
        }

        return redirect()->route('cars')->with('errors_toast', 'Não tem permissão para apagar esse carro!');
    }

    public function update(Request $request)
    {

        $this->validate($request, [
            'car_id' => 'exists:cars,id',
            'car_brand' => 'nullable|string|max:250',
            'car_model' => 'nullable|string|max:250',
            'car_year' => 'nullable|numeric',
            'car_month' => 'nullable|numeric',
            'car_speed_max' => 'nullable|numeric',
            'car_rpm_max' => 'nullable|numeric',
            'car_intake_temp_max' => 'nullable|numeric',
            'car_speed_threshold' => 'nullable|numeric',
            'car_rpm_threshold' => 'nullable|numeric',
            'car_intake_temp_threshold' => 'nullable|numeric',
        ]);

        $car = Car::findOrFail($request->car_id);

        $car->brand = $request->car_brand;
        $car->model = $request->car_model;
        $car->year = $request->car_year;
        $car->month = $request->car_month;

        $car->speed_max = $request->car_speed_max;
        $car->rpm_max = $request->car_rpm_max;
        $car->intake_temp_max = $request->car_intake_temp_max;

        $car->speed_threshold = $request->car_speed_threshold;
        $car->rpm_threshold = $request->car_rpm_threshold;
        $car->intake_temp_threshold = $request->car_intake_temp_threshold;

        $car->save();

        return redirect()->route('cars')->with('status', 'Carro alterado com sucesso!');
    }

    public function carConfigs(Car $car)
    {
        return $car;
    }

}
