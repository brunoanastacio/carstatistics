<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($role)
    {
        return $this->role == $role;
    }

    public function cars()
    {
        return $this->hasMany('App\Car');
    }

    public function logs()
    {
        return $this->hasManyThrough('App\Log', 'App\Car');
    }

    public function lastLogs($number_results)
    {
        return $this->logs()->latest()->take($number_results)->get();
    }

}
