@extends('users._layouts._main', ['active_menu' => 'logs', 'nav_menu' => 'dashboard'] )

@section('title', $title)

@section('css')
@endsection

@section('menu.content')

<div class="container">

	<div class="page-header">
		<h1>
			Logs <small>Todos os registos do carro <strong>{{$car->matriculation}}</strong> !</small>
		</h1>
	</div>

	@if( count($logs) )
		<div class="table-responsive">
		  <table id="data-table-logs" class="table table-striped table-bordered" cellspacing="0" width="100%">
		    <thead>
		      <tr>
		      	<th>Id</th>
		      	<th>Descrição</th>
		        <th>Valor</th>
		        <th>Unidade</th>
		        <th>Data</th>
		      </tr>
		    </thead>
		    <tbody>
		      @foreach ($logs as $log)
		        <tr>
		          <td>{{ $log->id }}</td>
		          <td>{{ $log->description }}</td>
		          <td>{{ $log->value }}</td>
		          <td>{{ $log->key }}</td>
		          <td>{{ $log->created_at }}</td>
		        </tr>
		      @endforeach
		    </tbody>
		  </table>
		</div>

	@else
		<p>Não existem logs disponiveis!</p>
	@endif

</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function() {
			var options =  {
				"order": [[ 4, "desc" ]],
				"language": {
					"lengthMenu": "Mostrar _MENU_ resultados por página",
					"zeroRecords": "Sem resultados para mostrar",
					"info": "Página _PAGE_ de _PAGES_",
					"infoEmpty": "Sem resultados disponiveis",
					"infoFiltered": "(Filtrado a partir de _MAX_ resultados)",
					"search":         "Pesquisa:",
					"paginate": {
						"first":      "Primeiro",
						"last":       "Último",
						"next":       "Próximo",
						"previous":   "Anterior"
					},
				}
			};

			var table_logs = $('#data-table-logs').DataTable(options);
		});
	</script>
@endsection
