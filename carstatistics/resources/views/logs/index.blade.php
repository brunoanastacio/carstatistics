@extends('users._layouts._main', ['active_menu' => 'logs', 'nav_menu' => 'dashboard'] )

@section('title', $title)

@section('css')
@endsection

@section('menu.content')

<div class="">

	<div class="page-header">
		<h1>
			Logs <small>Últimos 1000 registos dos seus carros!</small>
		</h1>
	</div>


	<form>
		<fieldset>
			<div class="form-group">
				<label>Carro:</label>
				<select id="select_car" class="form-control">
					<option value="all">Todos os carros</option>
					@foreach ($cars as $car)
						<option value="{{$car->id}}">{{$car->matriculation}}</option>
					@endforeach
				</select>
			</div>
		</fieldset>
	</form>

	@if( count($logs) )
		<div class="table-responsive">
		  <table id="data-table-logs" class="table table-striped table-bordered" cellspacing="0" width="100%">
		    <thead>
		      <tr>
		      	<th>Id</th>
		      	<th>Id do carro</th>
		      	<th>Matricula</th>
		      	<th>Descrição</th>
		        <th>Valor</th>
		        <th>Unidade</th>
		        <th>Data</th>
		      </tr>
		    </thead>
		    <tbody>
		      @foreach ($logs as $log)
		        <tr>
		          <td>{{ $log->id }}</td>
		          <th>{{ $log->car_id }}</th>
		          <th>{{ $log->car->matriculation }}</th>
		          <td>{{ $log->description }}</td>
		          <td>{{ $log->value }}</td>
		          <td>{{ $log->key }}</td>
		          <td>{{ $log->created_at }}</td>
		        </tr>
		      @endforeach
		    </tbody>
		  </table>
		</div>

	@else
		<p>Não existem logs disponiveis!</p>
	@endif

</div>
@endsection

@section('javascript')
	<script type="text/javascript">

		$.fn.dataTable.ext.search.push(
			function( settings, data, dataIndex ) {
				var value = $('#select_car').val();
				if(value == "all"){
					return true;
				}
				if(value ==  data[1]){
					return true;
				}
				return false;
			}
		);

		$(document).ready(function() {
			var options =  {
				"order": [[ 6, "desc" ]],
				"language": {
					"lengthMenu": "Mostrar _MENU_ resultados por página",
					"zeroRecords": "Sem resultados para mostrar",
					"info": "Página _PAGE_ de _PAGES_",
					"infoEmpty": "Sem resultados disponiveis",
					"infoFiltered": "(Filtrado a partir de _MAX_ resultados)",
					"search":         "Pesquisa:",
					"paginate": {
						"first":      "Primeiro",
						"last":       "Último",
						"next":       "Próximo",
						"previous":   "Anterior"
					},
				}
			};

			var table_logs = $('#data-table-logs').DataTable(options);

			$('#select_car').change(function() {
				console.log("change");
				table_logs.draw();
			});
		});
	</script>
@endsection
