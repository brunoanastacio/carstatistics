@extends('users._layouts._main', ['active_menu' => 'cars', 'nav_menu' => 'dashboard'] )

@section('title', $title)

@section('css')
@endsection

@section('menu.content')

<div class="">

	<div class="page-header">
		<h1>
			Carros <small>Informação dos seus carros</small>
			<div class="pull-right">
				<a class="btn btn-success" href="{{ route('cars.create') }}" role="button">Registar Carro</a>
			</div>
		</h1>
	</div>

	@if( count($cars) )
		<div class="table-responsive">
		  <table id="data-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
		    <thead>
		      <tr>
		      	<th>Id</th>
		        <th>Matrícula</th>
		        <th>Marca</th>
		        <th>Modelo</th>
		        <th>Ano</th>
		        <th>Mês</th>
		        <th>Acções</th>
		      </tr>
		    </thead>
		    <tbody>
		      @foreach ($cars as $car)
		        <tr>
		          	<td>{{ $car->id }}</td>
		         	<td>{{ $car->matriculation }}</td>
			        <td>{{ $car->brand }}</td>
			        <td>{{ $car->model }}</td>
			        <td>{{ $car->year }}</td>
			        <td>{{ $car->month }}</td>
			        <td>
			          	<a class="btn-sm btn-danger" href="{{ route('cars.delete', ['car' => $car->id]) }}" role="button">Apagar</a>
			          	<a data-toggle="modal" type="button" data-target="#editCarModal" data-car="{{ $car }}" class="btn-sm btn-warning">
								Editar</a>
			          	<a class="btn-sm btn-success" href="{{ route('cars.logs', ['car' => $car->id]) }}" role="button">Logs</a>
			        </td>
		        </tr>
		      @endforeach
		    </tbody>
		  </table>
		</div>

	@else
		<p>Não tem carros registados!</p>
	@endif

	<div class="modal fade" id="editCarModal" role="dialog" aria-labelledby="exampleModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="exampleModalLabel">Grupo ID: </h4>
	      </div>
	      <form method="POST" action="{{  route('car.update') }}">
		      <div class="modal-body">
				{{ csrf_field() }}
				<div class="form-group">
					<label class="control-label">Marca: </label>
					<input id="inputBrand" name="car_brand" type="text" class="form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Modelo: </label>
					<input id="inputModel" name="car_model" type="text" class="form-control">
				</div>
				<div class="form-group">
				    <label>Ano: </label>
				    <input id="inputYear" type="number" class="form-control" name="car_year" placeholder="model" value="{{ old('year') }}" min="1960" max="2018">
				</div>
				<div class="form-group">
				    <label>Mês: </label>
				    <input id="inputMonth" type="number" class="form-control" name="car_month" placeholder="model" value="{{ old('month') }}" min="1" max="12">
				</div>
				<input id="inputCaID" name="car_id" type="hidden" class="form-control">

				<div class="row">
            		<div class="col-md-4">
              			<div class="form-group">
							<label>Limite máximo velocidade: </label>
				    		<input id="inputMaxSpeed" type="number" class="form-control" name="car_speed_max" placeholder="120" value="{{ old('speed_max') }}" min="1" max="280">
						</div>
            		</div>
            		<div class="col-md-4">
             			<div class="form-group">
							<label>Limite máximo de rotações p/minuto: </label>
				    		<input id="inputMaxRpm" type="number" class="form-control" name="car_rpm_max" placeholder="4500" value="{{ old('rpm_max') }}" min="1" max="6000">
						</div>
           			</div>
           			<div class="col-md-4">
             		 	<div class="form-group">
							<label>Temperatura máxima do ar: </label>
				    		<input id="inputMaxIntakeTemp" type="number" class="form-control" name="car_intake_temp_max" placeholder="28" value="{{ old('intake_temp_max') }}" min="1" max="60">
						</div>
           			</div>
         		 </div>
         		 <div class="row">
            		<div class="col-md-4">
              			<div class="form-group">
							<label>Limiar de velocidade: </label>
				    		<input id="inputThresholdSpeed" type="number" class="form-control" name="car_speed_threshold" placeholder="10" value="{{ old('speed_threshold') }}" min="5" max="280">
						</div>
            		</div>
            		<div class="col-md-4">
             			<div class="form-group">
							<label>Limiar de rotações p/minuto: </label>
				    		<input id="inputThresholdRpm" type="number" class="form-control" name="car_rpm_threshold" placeholder="500" value="{{ old('rpm_threshold') }}" min="250" max="6000">
						</div>
           			</div>
           			<div class="col-md-4">
             		 	<div class="form-group">
							<label>Limiar de temperatura do ar: </label>
				    		<input id="inputThresholdIntakeTemp" type="number" class="form-control" name="car_intake_temp_threshold" placeholder="5" value="{{ old('intake_temp_threshold') }}" min="5" max="60">
						</div>
           			</div>
         		 </div>

		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		        <button type="submit" class="btn btn-danger">Editar Carro</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>

</div>
@endsection

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function() {
			$('#data-table').DataTable( {
				"language": {
					"lengthMenu": "Mostrar _MENU_ resultados por página",
					"zeroRecords": "Sem resultados para mostrar",
					"info": "Página _PAGE_ de _PAGES_",
					"infoEmpty": "Sem resultados disponiveis",
					"infoFiltered": "(Filtrado a partir de _MAX_ resultados)",
					"search":         "Pesquisa:",
					"paginate": {
						"first":      "Primeiro",
						"last":       "Último",
						"next":       "Próximo",
						"previous":   "Anterior"
					},
				},
				"columnDefs": [ {
				    "targets": [ -1 ],
				    "sortable": false,
				} ]
			});

			$('#editCarModal').on('show.bs.modal', function (event) {
				var a_link = $(event.relatedTarget) // Button that triggered the modal
				var car = a_link.data('car')

				console.log(car);

				var modal = $(this)
				modal.find('.modal-title').text('Car ID: '+ car['id'] +' |  Matrícula: ' + car['matriculation']);

				$('#inputBrand').val(car['brand']);
				$('#inputModel').val(car['model']);
				$('#inputYear').val(car['year']);
				$('#inputMonth').val(car['month']);
				$('#inputCaID').val(car['id']);

				$('#inputMaxSpeed').val(car['speed_max']);
				$('#inputMaxRpm').val(car['rpm_max']);
				$('#inputMaxIntakeTemp').val(car['intake_temp_max']);
				$('#inputThresholdSpeed').val(car['speed_threshold']);
				$('#inputThresholdRpm').val(car['rpm_threshold']);
				$('#inputThresholdIntakeTemp').val(car['intake_temp_threshold']);

			});

		});
	</script>
@endsection
