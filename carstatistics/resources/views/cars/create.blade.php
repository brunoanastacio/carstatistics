@extends('users._layouts._main', ['active_menu' => 'cars', 'nav_menu' => 'dashboard'] )

@section('title', $title)


@section('menu.content')

<div class="row">
	<div class="col-md-12">
		<div class="page-header">
			<h1>
				Registar um novo carro
			</h1>
		</div>
		<form method="POST" action="{{ route('cars.store') }}">
		  	{{ csrf_field() }}

			<div class="form-group">
			    <label>Matricula: </label>
			    <input type="text" class="form-control" name="matriculation" placeholder="13-BB-98" value="{{ old('matriculation') }}">
			</div>
			<div class="form-group">
			    <label>Marca: </label>
			    <input type="text" class="form-control" name="brand" placeholder="honda" value="{{ old('brand') }}">
			</div>
			<div class="form-group">
			    <label>Modelo: </label>
			    <input type="text" class="form-control" name="model" placeholder="model" value="{{ old('model') }}">
			</div>
			<div class="form-group">
			    <label>Ano: </label>
			    <input type="number" class="form-control" name="year" placeholder="model" value="{{ old('year') }}" min="1960" max="2018">
			</div>
			<div class="form-group">
			    <label>Mês: </label>
			    <input type="number" class="form-control" name="month" placeholder="model" value="{{ old('month') }}" min="1" max="12">
			</div>
			<div class="form-group">
				<a class="btn btn-danger" href="{{ route('cars') }}">Cancelar</a>
				<button type="submit" class="btn btn-default">Submeter</button>
		 	</div>
		</form>
	</div>
</div>

@endsection

@section('javascript')

<script type="text/javascript">

</script>
@endsection
