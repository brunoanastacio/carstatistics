@extends('users._layouts._main', ['active_menu' => '', 'nav_menu' => ''] )

@section('title', $title)

@section('content')

<div class="container">
    <div class="row">
       	<h1 class="">Perfil de {{ $user->name }}</h1>
    </div>

    <div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>
					Alterar utilizador
				</h1>
			</div>
			<form method="POST" action="{{ route('user.update', ['user' => $user->id]) }}">
			  	{{ csrf_field() }}

				<div class="form-group">
				    <label>Nome: </label>
				    <input type="text" class="form-control" name="name" placeholder="Bruno Anastácio" value="{{ $user->name }}">
				</div>
				<div class="form-group">
				    <label>Email: </label>
				    <input type="email" class="form-control" name="email" placeholder="bruno@gmail.com" value="{{ $user->email }}">
				</div>
				<div class="form-group">
					<a class="btn btn-danger" href="{{ route('dashboard') }}">Cancelar</a>
					<button type="submit" class="btn btn-success">Alterar perfil</button>
			 	</div>
			</form>
		</div>
	</div>

</div>

@endsection
