@extends('users._layouts._main', ['active_menu' => 'dashboard', 'nav_menu' => 'dashboard'] )

@section('title', $title)

@section('css')
	<style type="text/css">
        .chart{
            width: 50%;
            margin: 0 auto;
            display: table;
        }

    	svg:first-child > g > text[text-anchor~=middle]{
    		font-size:20px;
		}
	</style>
@endsection

@section('menu.content')

<div class="">
    <div class="row">
       	<h1 class="">Dashboard</h1>
    </div>
    <div class="row">
       	<h3 id="car" class=""></h3>
    </div>
    <div class="row">
       	<div class="col-md-4">
       		<h4 style="text-align: center;">Velocidade</h4>
			<div id="speed_div" class="chart"></div>
		</div>
		<div class="col-md-4">
       		<h4 style="text-align: center;">Rotações por minuto</h4>
			<div id="rpm_div" class="chart"></div>
		</div>
		<div class="col-md-4">
       		<h4 style="text-align: center;">Temperatura do Ar</h4>
			<div id="temp_div" class="chart"></div>
		</div>
    </div>

</div>
@endsection

@section('javascript')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
	<script type="text/javascript">
		var user_id = {{Auth::user()->id}};

		$(document).ready(function() {
			var speedData; var speedChart; var speedOptions;
			var rpmData; var rpmChart; var rpmOptions;
			var tempData; var tempChart; var tempOptions;

			google.charts.load('current', {'packages':['gauge']});
		    google.charts.setOnLoadCallback(drawChart);

			//Pusher.logToConsole = true;

		    var pusher = new Pusher('d2ff6b2aa855b812db52', {
		      	cluster: 'eu',
		      	encrypted: true
		    });

		    var channel = pusher.subscribe('channel-'+user_id);
		    console.log('channel-'+user_id);

		    channel.bind('status', function(data) {
		    	if(data.brand != ''){
		    		$('#car').text(data.brand + ' ' + data.model + ' - ' + data.matriculation);
		    	}
		    	if(data.key == 'speed'){
		    		updateChartSpeed(data.value);
		    	}
		    	if(data.key == 'rpm'){
		    		updateChartRpm(data.value/1000);
		    	}
		    	if(data.key == 'intake_temp'){
		    		updateChartTemp(data.value);
		    	}
		    });

		    function drawChart() {

		        speedData = google.visualization.arrayToDataTable([
		          ['Label', 'Value'],
		          ['Km/h', 0],
		        ]);

		        rpmData = google.visualization.arrayToDataTable([
		          ['Label', 'Value'],
		          ['Rpm*1000', 0],
		        ]);

		        tempData = google.visualization.arrayToDataTable([
		          ['Label', 'Value'],
		          ['Temp Cº', 0],
		        ]);

		        speedOptions = {
		          width: 300, height: 300,
		          redFrom: 260, redTo: 280,
		          yellowFrom:240, yellowTo: 260,
		          minorTicks: 10,
		          max:280
		        };

		        rpmOptions = {
		          width: 300, height: 300,
		          redFrom: 5, redTo: 6,
		          minorTicks: 10,
		          max:6
		        };

		        tempOptions = {
		          width: 200, height: 200,
		          redFrom: 75, redTo: 80,
		          yellowFrom:70, yellowTo: 75,
		          minorTicks: 5,
		          max:80,
		          min:0
		        };

		        speedChart = new google.visualization.Gauge(document.getElementById('speed_div'));
		        rpmChart = new google.visualization.Gauge(document.getElementById('rpm_div'));
		        tempChart = new google.visualization.Gauge(document.getElementById('temp_div'));
		        speedChart.draw(speedData, speedOptions);
		        rpmChart.draw(rpmData, rpmOptions);
		        tempChart.draw(tempData, tempOptions);
		    }

		    function updateChartSpeed(value) {
		        speedData.setValue(0, 1, value);
		        speedChart.draw(speedData, speedOptions);
		    }
		    function updateChartRpm(value) {
		        rpmData.setValue(0, 1, value);
		        rpmChart.draw(rpmData, rpmOptions);
		    }
		    function updateChartTemp(value) {
		        tempData.setValue(0, 1, value);
		        tempChart.draw(tempData, tempOptions);
		    }

		});
	</script>
	<script></script>
@endsection
