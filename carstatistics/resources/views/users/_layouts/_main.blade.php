@extends('layouts._nav_menu')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			<div class="list-group">
			  <span class="list-group-item custom-list-group-heading active">
			    <strong>Menu</strong>
			  </span>
			  <a href="{{ route('dashboard') }}" class="list-group-item custom-list-group-links
			  	{{ $active_menu == 'dashboard' ? 'custom-active-item-list-group' : ''}}">
			  	Dashboard
			  </a>
			  <a href="{{ route('cars') }}" class="list-group-item custom-list-group-links
			  	{{ $active_menu == 'cars' ? 'custom-active-item-list-group' : ''}}">
			  	Carros</a>
			  <a href="{{ route('logs') }}" class="list-group-item custom-list-group-links
			  {{ $active_menu == 'logs' ? 'custom-active-item-list-group' : ''}}">
			  	Logs</a>
			</div>
		</div>
		<div class="col-md-10">
			@yield('menu.content')
		</div>
	</div>
</div>
@endsection
