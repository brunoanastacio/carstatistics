@extends('layouts.app')

@section('navigation')
<ul class="nav navbar-nav">
	@if(Auth::user())
	<li {{ $nav_menu == 'dashboard' ? 'class=custom-active-nav' : '' }}>
		<a href="{{ route('dashboard') }}">Dashboard</a>
	</li>
	@endif
</ul>

@endsection
