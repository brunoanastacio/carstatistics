@extends('layouts._nav_menu', ['nav_menu' => 'home'])

@section('title', $title)

@section('css')
	<style>
		.center-image {
		    margin: auto;
		    width: 50%;
		    border: 3px solid blue;
		    padding: 10px;
		    width: 850px;
		    height: 600px;
		}

		hr.hr-style {
			height: 30px;
			border-style: solid;
			border-color: #8c8b8b;
			border-width: 1px 0 0 0;
			border-radius: 20px;
		}

	</style>
@endsection

@section('content')

<div class="container">

    <div class="row">
	    <h1><strong>Car Statistics</strong></h1>
	    <h3 class="lead"> Veja informação do seu carro em tempo real em qualquer lugar!</h3>
	    <hr class="hr-style">
    </div>

    <div class="row">
		<img class="media-object center-image" src="https://cdn-images-1.medium.com/max/1600/1*4EsJxUhS4w73rtxlDYK2rQ.jpeg">
    </div>

</div>

@endsection
